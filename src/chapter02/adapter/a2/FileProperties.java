package chapter02.adapter.a2;

import java.io.*;
import java.util.Properties;

/**
 * FileProperties    相当于adapter 适配器角色
 * Properties    相当于adaptee 被适配角色
 * FileIO    相当于Target 目标对象角色
 */
public class FileProperties extends Properties implements FileIO {
    public void readFromFile(String filename) throws IOException {
        load(new FileInputStream(filename));
    }
    public void writeToFile(String filename) throws IOException {
        store(new FileOutputStream(filename), "written by FileProperties");
    }
    public void setValue(String key, String value) {
        setProperty(key, value);
    }
    public String getValue(String key) {
        return getProperty(key, "");
    }
}
