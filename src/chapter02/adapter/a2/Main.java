package chapter02.adapter.a2;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        //获取系统根目录    C:\Users\MFC
        System.out.println(System.getProperty("user.home"));
        //获取工程目录    D:\my.uinfor.com\MyStudy\图解设计模式\graphic-design-mode
        System.out.println(System.getProperty("user.dir"));
        //获取当前工程目录    D:\my.uinfor.com\MyStudy\图解设计模式\graphic-design-mode\.
        System.out.println(new File(".").getAbsoluteFile());

        FileIO f = new FileProperties();
        try {
            //在工程目录下读取文件出错    java.io.FileNotFoundException: file.txt (系统找不到指定的文件。)
            //f.readFromFile("file.txt");
            f.readFromFile("./src/chapter02/adapter/a2/file.txt");
            f.setValue("year", "2004");
            f.setValue("month", "4");
            f.setValue("day", "21");
            //默认输出文件在工程目录下    D:\my.uinfor.com\MyStudy\图解设计模式\graphic-design-mode\newfile.txt
            //f.writeToFile("newfile.txt");
            f.writeToFile("./src/chapter02/adapter/a2/newfile.txt");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
