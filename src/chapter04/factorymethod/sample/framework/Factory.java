package chapter04.factorymethod.sample.framework;

/**
 * 工厂
 */
public abstract class Factory {
    /**
     * Template Method模板方法
     * @param owner
     * @return
     */
    public final Product create(String owner) {
        Product p = createProduct(owner);
        registerProduct(p);
        return p;
    }

    /**
     * 生产产品
     * @param owner
     * @return
     */
    protected abstract Product createProduct(String owner);

    /**
     * 注册产品
     * @param product
     */
    protected abstract void registerProduct(Product product);
}
