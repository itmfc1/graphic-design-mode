package chapter04.factorymethod.sample.framework;

/**
 * 产品
 */
public abstract class Product {
    public abstract void use();
}
