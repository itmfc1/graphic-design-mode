package chapter04.factorymethod.a2.framework;

public abstract class Product {
    public abstract void use();
}
