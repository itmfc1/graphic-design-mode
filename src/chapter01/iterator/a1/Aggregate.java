package chapter01.iterator.a1;

public interface Aggregate {
    public abstract Iterator iterator();
}
