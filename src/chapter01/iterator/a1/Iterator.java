package chapter01.iterator.a1;

public interface Iterator {
    public abstract boolean hasNext();
    public abstract Object next();
}
