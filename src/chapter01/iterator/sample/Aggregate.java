package chapter01.iterator.sample;

/**
 * 表示集合的接口
 **/
public interface Aggregate {
    public abstract Iterator iterator();
}
