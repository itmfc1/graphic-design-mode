package chapter01.iterator.sample;

/**
 * 遍历集合的接口
 **/
public interface Iterator {
    public abstract boolean hasNext();
    public abstract Object next();
}
