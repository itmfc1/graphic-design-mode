# 图解设计模式

#### 介绍

https://www.ituring.com.cn/book/1811

#### 创建目录

```
md chapter01 chapter02 chapter03 chapter04 chapter05 chapter06 chapter07 chapter08 chapter09

md chapter10 chapter11 chapter12 chapter13 chapter14 chapter15 chapter16 chapter17 chapter18

md chapter19 chapter20 chapter21 chapter22 chapter23 
```

#### 目录结构

```
└─src                 
    ├─chapter01    第01章 Iterator模式-------------------迭代器
    ├─chapter02    第02章 Adapter模式--------------------适配器
    ├─chapter03    第03章 Template Method模式------------模板方法
    ├─chapter04    第04章 Factory Method模式-------------工厂方法
    ├─chapter05    第05章 Singleton模式------------------单例
    ├─chapter06    第06章 Prototype模式------------------原型
    ├─chapter07    第07章 Builder模式--------------------建造者
    ├─chapter08    第08章 Abstract Factory模式-----------抽象工厂
    ├─chapter09    第09章 Bridge模式---------------------桥接
    ├─chapter10    第10章 Strategy模式-------------------策略
    ├─chapter11    第11章 Composite模式------------------组合
    ├─chapter12    第12章 Decorator模式------------------装饰
    ├─chapter13    第13章 Visitor模式--------------------访问者
    ├─chapter14    第14章 Chain of Responsibility模式----责任链
    ├─chapter15    第15章 Facade模式---------------------外观
    ├─chapter16    第16章 Mediator模式-------------------中介者
    ├─chapter17    第17章 Observer模式-------------------观察着
    ├─chapter18    第18章 Memento模式--------------------备忘录
    ├─chapter19    第19章 State模式----------------------状态
    ├─chapter20    第20章 Flyweight模式------------------享元
    ├─chapter21    第21章 Proxy模式----------------------代理
    ├─chapter22    第22章 Command模式--------------------命令
    └─chapter23    第23章 Interpreter模式----------------解释器
```

#### 核心类容

```
第1部分：适应设计模式
    第1章~第2章
第2部分：交给子类
    第3章~第4章
第3部分：生成实例
    第5章~8章
第4部分：分开考虑
    第9章~10章
第5部分：一致性
    第11章~12章
第6部分：访问数据结构
    第13章~第14章
第7部分：简单化
    第15章~第16章
第8部分：管理状态
    第17章~第19章
第9部分：避免浪费
    第20章~第21章
第10部分：用类来表现
    第22章~第23章        
```

#### 其他说明

各个目录的意义如下。

    src/模式名/sample      示例程序
    src/模式名/q☆...      练习题中的代码清单（☆是练习题编号）
    src/模式名/a☆...      练习解答中的代码清单（☆是练习题编号）

